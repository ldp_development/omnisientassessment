﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assessment
{
    public class Factory
    {
        public Machine CreateMachine(string config)
        {
            switch (config)
            {
                default:
                case "USD":
                    return new Machine(1, 5, 10, 25);
                case "GBP":
                    return new Machine(1, 2, 5, 10, 25, 50);
            }
        }
    }
}
