﻿using System;

namespace Assessment
{
    class Program
    {
        private static Machine machine;

        static void Main(string[] args)
        {
            Begin();
        }

        public static void Begin()
        {
            // create the machine
            var config = "GBP"; // USD or GBP
            machine = new Factory().CreateMachine(config);

            // set up purchase amount
            decimal purchase_amount = GetPurchaseAmt();
            Console.WriteLine("**********************************");
            Console.WriteLine("Purchase amount is {0}", purchase_amount);

            try
            {
                // Try make payment
                machine.Pay(purchase_amount);
                // to start process again as machine should keep working
                Begin();
            } catch (Exception ex)
            {
                Console.WriteLine($"Payment failed: {ex.Message}");
                // to start process again as machine should keep working
                Begin();
            }
        }

        private static decimal GetPurchaseAmt()
        {
            Random rnd = new Random();
            double rnd_value = rnd.NextDouble() + rnd.Next(2, 5);
            return Convert.ToDecimal(Math.Round(rnd_value, 2));
        }
    }
}
