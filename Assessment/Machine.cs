﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assessment
{
    public class Machine
    {
        #region Properties
        private int[] Denomination;
        #endregion

        #region Ctor
        public Machine(params int[] denom)
        {
            this.Denomination = denom;
        }
        #endregion

        #region Public Methods
        public int[] CalculateChange(decimal purchaseAmount, decimal tenderAmount)
        {
            // just in case duplicate denominations provided and unordered
            var structureList = this.Denomination
                .Distinct()
                .ToList();
            
            // get change to provide
            var change_amount = (tenderAmount - purchaseAmount)*100;

            // need to start loop, keep adding change till no more change required
            var change_list = new List<int>();
            while(change_amount > 0)
            {
                // find maximum available change to use
                // remove the max change available from change amount 
                // add change to the list of change to be provided
                var change = structureList.Where(x => x <= change_amount).Max();
                change_amount -= change;
                change_list.Add(change);
            }

            return change_list.ToArray();
        }

        public void Pay(decimal purchase_amount)
        {
            // request tender amount that must be equal to or greater than purchase amount
            Console.WriteLine("Provide Tender Amount");
            var tender_response = Console.ReadLine();
            // check if full stop used as decimals require commas, but we in SA use full stops 
            tender_response = tender_response.Replace('.', ',');
            decimal tender_amount;
            var is_tender = decimal.TryParse(tender_response, out tender_amount);
            
            if (!is_tender)
            {
                throw new Exception("Invalid payment amount provided");
            }

            if (tender_amount < purchase_amount)
            {
                throw new Exception("Insufficient funds provided");
            }

            var change = this.CalculateChange(purchase_amount, tender_amount);
            this.PublishResult(change);
        }
        #endregion

        #region Private Methods
        public void PublishResult(int[] change)
        {

            Console.WriteLine("Change provided");
            Console.WriteLine("**********************************");
            if (change.Length <= 0)
            {
                Console.WriteLine("No Change needed");
                return;
            }
            
            foreach (var item in change)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("**********************************");
        }
        #endregion
    }
}
